class Board {
    constructor() {
        this._board = "";
        this._boardMapped = "";
    }

    get size() {
        return Math.floor(Math.sqrt(this._board.length));
    }

    get size_length() {
        return this._board.length;
    }

    findElement(elementType) {
        const foundPoints = this._getMappedBoard()
            .filter(element => element.type === elementType)
            .map(element => element.coordinates);

        return foundPoints[0] || null;
    }

    findFirstElement(...elementTypes) {
        const element = this._getMappedBoard().find(element => {
            return elementTypes.includes(element.type);
        });

        if (!element) {
            return null;
        }

        return element.coordinates;
    }

    findAllElements(...elementTypes) {
        return this._getMappedBoard().reduce((points, element, index) => {
            if (elementTypes.includes(element.type)) {
                points.push(element.coordinates);
            }

            return points;
        }, []);
    }

    getElementAt(point) {
        const element = this._getMappedBoard().find(element => {
            return element.coordinates.equals(point);
        });

        if (!element) {
            return null;
        }

        return element.type;
    }

    hasElementAt(elementType, point) {
        return this.getElementAt(point) === elementType;
    }

    amIEvil() {
        return this.getMyHead() === ELEMENTS.HEAD_EVIL;
    }

    amIFlying() {
        return this.getMyHead() === ELEMENTS.HEAD_FLY;
    }

    getWalls() {
        return this.findAllElements(ELEMENTS.WALL);
    }

    getStones() {
        return this.findAllElements(ELEMENTS.STONE);
    }

    getApples() {
        return this.findAllElements(ELEMENTS.APPLE);
    }

    getGold() {
        return this.findAllElements(ELEMENTS.FURY_PILL);
    }

    getFuryPills() {
        return this.findAllElements(ELEMENTS.FURY_PILL);
    }

    getStartPoints() {
        return this.findAllElements(ELEMENTS.START_FLOOR);
    }

    getFlyingPills() {
        return this.findAllElements(ELEMENTS.FLYING_PILL);
    }

    getMyHead() {
        const headElementTypes = [
            "HEAD_DEAD",
            "HEAD_DOWN",
            "HEAD_UP",
            "HEAD_LEFT",
            "HEAD_RIGHT",
            "HEAD_EVIL",
            "HEAD_FLY",
            "HEAD_SLEEP"
        ].map(elementName => ELEMENTS[elementName]);

        return this.findFirstElement(...headElementTypes);
    }

    getBarriers() {
        const elementTypes = [
            "WALL",
            "START_FLOOR",
            "ENEMY_HEAD_SLEEP",
            "ENEMY_TAIL_INACTIVE",
            "TAIL_INACTIVE",
            "STONE"
        ].map(elementName => ELEMENTS[elementName]);

        return this.findAllElements(...elementTypes);
    }

    isBarrierAt(point) {
        return !!this.getBarriers().find(barrierPoint =>
            barrierPoint.equals(point)
        );
    }

    update(raw) {
        this._board = raw.replace("board=", "");
        this._boardMapped = this._board.split("").map((element, index) => {
            return {type: element, coordinates: this.getPointByShift(index)};
        });
    }

    _getMappedBoard() {
        return this._boardMapped;
    }

    getPointByShift(shift) {
        return new Point(shift % this.size, Math.floor(shift / this.size));
    }

    toString() {
        const lineRegExp = new RegExp(`(.{${this.size}})`, "g");
        return this._board.replace(lineRegExp, "$1\n");
    }

    getLengthMyShape() {
        const headElementTypes = [
            "HEAD_DEAD",
            "HEAD_DOWN",
            "HEAD_UP",
            "HEAD_LEFT",
            "HEAD_RIGHT",
            "HEAD_EVIL",
            "HEAD_FLY",
            "HEAD_SLEEP",

            "TAIL_END_DOWN",
            "TAIL_END_LEFT",
            "TAIL_END_UP",
            "TAIL_END_RIGHT",
            "TAIL_INACTIVE",

            "BODY_HORIZONTAL",
            "BODY_VERTICAL",
            "BODY_LEFT_DOWN",
            "BODY_LEFT_UP",
            "BODY_RIGHT_DOWN",
            "BODY_RIGHT_UP",
        ].map(elementName => ELEMENTS[elementName]);

        return this.findAllElements(...headElementTypes).length;

    }
}