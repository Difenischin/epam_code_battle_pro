const outputElement = document.getElementById("text");
// const url = "http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/nwqpn6owr1dpj2e5k2ky?code=5959247023414867227";
const url = "http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/am152op6g7b8tsxxytb9?code=2491421033552508375&gameName=snakebattle";


const ELEMENTS_ESTIMATE = {
    [ELEMENTS.NONE]: 1, // пустое место
    [ELEMENTS.WALL]: -100, // а это стенка
    [ELEMENTS.START_FLOOR]: -100, // место старта змей
    [ELEMENTS.OTHER]: -100, // этого ты никогда не увидишь :)

    [ELEMENTS.APPLE]: 15, // яблоки надо кушать от них становишься длинее
    [ELEMENTS.STONE]: -19, // а это кушать не стоит - от этого укорачиваешься
    [ELEMENTS.FLYING_PILL]: 15, // таблетка полета - дает суперсилы
    [ELEMENTS.FURY_PILL]: 30, // таблетка ярости - дает суперсилы
    [ELEMENTS.GOLD]: 10, // золото - просто очки

    // голова твоей змеи в разных состояниях и направлениях
    [ELEMENTS.HEAD_DOWN]: 0,
    [ELEMENTS.HEAD_LEFT]: 0,
    [ELEMENTS.HEAD_RIGHT]: 0,
    [ELEMENTS.HEAD_UP]: 0,
    [ELEMENTS.HEAD_DEAD]: 0, // этот раунд ты проиграл
    [ELEMENTS.HEAD_EVIL]: 0, // ты скушал таблетку ярости
    [ELEMENTS.HEAD_FLY]: 0, // ты скушал таблетку полета
    [ELEMENTS.HEAD_SLEEP]: 0, // твоя змейка ожидает начала раунда

    // хвост твоей змейки
    [ELEMENTS.TAIL_END_DOWN]: 0,
    [ELEMENTS.TAIL_END_LEFT]: 0,
    [ELEMENTS.TAIL_END_UP]: 0,
    [ELEMENTS.TAIL_END_RIGHT]: 0,
    [ELEMENTS.TAIL_INACTIVE]: 0,

    // туловище твоей змейки
    [ELEMENTS.BODY_HORIZONTAL]: -20,
    [ELEMENTS.BODY_VERTICAL]: -20,
    [ELEMENTS.BODY_LEFT_DOWN]: -20,
    [ELEMENTS.BODY_LEFT_UP]: -20,
    [ELEMENTS.BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.BODY_RIGHT_UP]: -20,

    // змейки противников
    [ELEMENTS.ENEMY_HEAD_DOWN]: -10,
    [ELEMENTS.ENEMY_HEAD_LEFT]: -10,
    [ELEMENTS.ENEMY_HEAD_RIGHT]: -10,
    [ELEMENTS.ENEMY_HEAD_UP]: -10,
    [ELEMENTS.ENEMY_HEAD_DEAD]: 0, // этот раунд противник проиграл
    [ELEMENTS.ENEMY_HEAD_EVIL]: 0, // противник скушал таблетку ярости
    [ELEMENTS.ENEMY_HEAD_FLY]: 0, // противник скушал таблетку полета
    [ELEMENTS.ENEMY_HEAD_SLEEP]: 0, // змейка противника ожидает начала раунда

    // хвосты змеек противников
    [ELEMENTS.ENEMY_TAIL_END_DOWN]: -5,
    [ELEMENTS.ENEMY_TAIL_END_LEFT]: -5,
    [ELEMENTS.ENEMY_TAIL_END_UP]: -5,
    [ELEMENTS.ENEMY_TAIL_END_RIGHT]: -5,
    [ELEMENTS.ENEMY_TAIL_INACTIVE]: -5,

    // туловище змеек противников
    [ELEMENTS.ENEMY_BODY_HORIZONTAL]: -20,
    [ELEMENTS.ENEMY_BODY_VERTICAL]: -20,
    [ELEMENTS.ENEMY_BODY_LEFT_DOWN]: -20,
    [ELEMENTS.ENEMY_BODY_LEFT_UP]: -20,
    [ELEMENTS.ENEMY_BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.ENEMY_BODY_RIGHT_UP]: -20
};


const ELEMENTS_ESTIMATE_BERSERK = {
    [ELEMENTS.NONE]: 1, // пустое место
    [ELEMENTS.WALL]: -100, // а это стенка
    [ELEMENTS.START_FLOOR]: -100, // место старта змей
    [ELEMENTS.OTHER]: -100, // этого ты никогда не увидишь :)

    [ELEMENTS.APPLE]: 15, // яблоки надо кушать от них становишься длинее
    [ELEMENTS.STONE]: 10, // а это кушать не стоит - от этого укорачиваешься
    [ELEMENTS.FLYING_PILL]: 15, // таблетка полета - дает суперсилы
    [ELEMENTS.FURY_PILL]: 35, // таблетка ярости - дает суперсилы
    [ELEMENTS.GOLD]: 10, // золото - просто очки

    // голова твоей змеи в разных состояниях и направлениях
    [ELEMENTS.HEAD_DOWN]: 0,
    [ELEMENTS.HEAD_LEFT]: 0,
    [ELEMENTS.HEAD_RIGHT]: 0,
    [ELEMENTS.HEAD_UP]: 0,
    [ELEMENTS.HEAD_DEAD]: 0, // этот раунд ты проиграл
    [ELEMENTS.HEAD_EVIL]: 0, // ты скушал таблетку ярости
    [ELEMENTS.HEAD_FLY]: 0, // ты скушал таблетку полета
    [ELEMENTS.HEAD_SLEEP]: 0, // твоя змейка ожидает начала раунда

    // хвост твоей змейки
    [ELEMENTS.TAIL_END_DOWN]: 0,
    [ELEMENTS.TAIL_END_LEFT]: 0,
    [ELEMENTS.TAIL_END_UP]: 0,
    [ELEMENTS.TAIL_END_RIGHT]: 0,
    [ELEMENTS.TAIL_INACTIVE]: 0,

    // туловище твоей змейки
    [ELEMENTS.BODY_HORIZONTAL]: -20,
    [ELEMENTS.BODY_VERTICAL]: -20,
    [ELEMENTS.BODY_LEFT_DOWN]: -20,
    [ELEMENTS.BODY_LEFT_UP]: -20,
    [ELEMENTS.BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.BODY_RIGHT_UP]: -20,

    // змейки противников
    [ELEMENTS.ENEMY_HEAD_DOWN]: 26,
    [ELEMENTS.ENEMY_HEAD_LEFT]: 26,
    [ELEMENTS.ENEMY_HEAD_RIGHT]: 26,
    [ELEMENTS.ENEMY_HEAD_UP]: 26,
    [ELEMENTS.ENEMY_HEAD_DEAD]: 0, // этот раунд противник проиграл
    [ELEMENTS.ENEMY_HEAD_EVIL]: 0, // противник скушал таблетку ярости
    [ELEMENTS.ENEMY_HEAD_FLY]: 0, // противник скушал таблетку полета
    [ELEMENTS.ENEMY_HEAD_SLEEP]: 0, // змейка противника ожидает начала раунда

    // хвосты змеек противников
    [ELEMENTS.ENEMY_TAIL_END_DOWN]: -1,
    [ELEMENTS.ENEMY_TAIL_END_LEFT]: -1,
    [ELEMENTS.ENEMY_TAIL_END_UP]: -1,
    [ELEMENTS.ENEMY_TAIL_END_RIGHT]: -1,
    [ELEMENTS.ENEMY_TAIL_INACTIVE]: -1,

    // туловище змеек противников
    [ELEMENTS.ENEMY_BODY_HORIZONTAL]: 25,
    [ELEMENTS.ENEMY_BODY_VERTICAL]: 25,
    [ELEMENTS.ENEMY_BODY_LEFT_DOWN]: 25,
    [ELEMENTS.ENEMY_BODY_LEFT_UP]: 25,
    [ELEMENTS.ENEMY_BODY_RIGHT_DOWN]: 25,
    [ELEMENTS.ENEMY_BODY_RIGHT_UP]: 25
};

const client = new GameClient(url, {
    onUpdate: board => {
        outputElement.value = board.toString();
    },

    log: message => {
        console.log(message);
        outputElement.value += message;
    }
});

client.run(gameAnt);


/*
двигаемся с текущей позиции на одну клетку

запоминаем куда идём

на каждом шаге есть возможность пойти в 3 направления
мы не может идти туда откуда пришли

каждую клетку оцениваем и запоминаем вес.
Если стена то прекращаем движенние.

Если нет идём дальше.

берём таких сто движений, находим среди нх максимально профитный путь и идём понему.

можно сразу добавить небольшую эвристику по поводу чтобы не идти в стену.

создаем пул муравьев

начинаем движение каждым маравьем.

если двигаемся запоменаем направление откуда сдвинулись
запоминаем вес текущей ячейки или ячейки где оказываемся.
получаем список доступных направлений
если их нету то присваем в счётчик отрицательный вес

если есть то идём дальше.


на каждом шаге выводить список доступных направлений


*/


let globalLastDirection = DIRECTIONS.RIGHT;
let globalBerserkLimitStep = 0;

function gameAnt(board) {
    try {
        const dateStart = performance.now();
        if (isBerserkMode(board)) {
            if (globalBerserkLimitStep === 0) {
                globalBerserkLimitStep += 10;
            }
            //потому то по другому невозможно динамически отслеживать сколько шагов в берсерк режиме осталось
            if (globalBerserkLimitStep > 0) {// в условиях есть побочный эффект. порядок важен
                globalBerserkLimitStep -= 1;
            }
            if (globalBerserkLimitStep < 0) {
                globalBerserkLimitStep = 0;
            }
        } else {
            globalBerserkLimitStep = 0;
        }

        const antColony = generateAntColony(board, 1500, globalLastDirection, globalBerserkLimitStep);
        const maxSteps = 12;
        for (let i = 0; i < maxSteps; i++) {
            for (let j = 0; j < antColony.length; j++) {
                antColony[j] = getAntStep(antColony[j], board);
                // if (performance.now() - dateStart > 800) {
                //     break;
                // }
            }
        }

        const bestAnt = getBestAnt(antColony);

        const isActing = false;
        console.log(bestAnt);
        const dateEnd = performance.now();
        console.log("Call to gameAnt took " + (dateEnd - dateStart) + " milliseconds.")
        console.log(bestAnt.profit)
        globalLastDirection = bestAnt.directionsGo[1];
        return new Action(bestAnt.directionsGo[1], isActing);
    } catch (e) {
        console.error(e);
        return new Action(DIRECTIONS.DOWN, false);
    }
}

function generateAntColony(board, countAnt, defaultDirection, globalBerserkLimitStep) {
    const antColony = [];

    for (let i = 0; i < countAnt; i++) {
        let ant = {
            path: [],
            directionsGo: [getLastDirection(board, defaultDirection)],
            currentPoint: board.getMyHead(),
            profit: 0,
            profitPositive: 0,
            done: false,
            donePositive: false,
            countStep: 0,
            berserkMode: isBerserkMode(board),
            berserkLimitStep: globalBerserkLimitStep,
            antLength: getAntLength(board)
        }
        antColony.push(ant);
    }
    return antColony;

    function getAntLength(board) {
        if (getAntLength.board !== board.toString()) {
            getAntLength.board = board.toString();
            getAntLength.instance = board.getLengthMyShape();
        }
        return getAntLength.instance;
    }
}

function getLastDirection(board, defaultDirection) {
    const currentElement = board.getElementAt(board.getMyHead());
    if (currentElement === ELEMENTS.HEAD_RIGHT) {
        return DIRECTIONS.RIGHT;
    }
    if (currentElement === ELEMENTS.HEAD_LEFT) {
        return DIRECTIONS.LEFT;
    }
    if (currentElement === ELEMENTS.HEAD_UP) {
        return DIRECTIONS.UP;
    }
    if (currentElement === ELEMENTS.HEAD_DOWN) {
        return DIRECTIONS.DOWN;
    }
    const currentPoint = board.getMyHead();
    const elementRight = board.getElementAt(currentPoint.shiftRight(1));
    const elementLeft = board.getElementAt(currentPoint.shiftLeft(1));
    const elementUp = board.getElementAt(currentPoint.shiftTop(1));
    const elementDown = board.getElementAt(currentPoint.shiftBottom(1));
    // туловище твоей змейки
    // BODY_HORIZONTAL: "═",
    //     BODY_VERTICAL: "║",
    //     BODY_LEFT_DOWN: "╗",
    //     BODY_LEFT_UP: "╝",
    //     BODY_RIGHT_DOWN: "╔",
    //     BODY_RIGHT_UP: "╚",
    if (elementRight === ELEMENTS.BODY_HORIZONTAL) {
        return DIRECTIONS.LEFT;
    }
    if (elementLeft === ELEMENTS.BODY_HORIZONTAL) {
        return DIRECTIONS.RIGHT;
    }
    if (elementUp === ELEMENTS.BODY_VERTICAL) {
        return DIRECTIONS.DOWN;
    }
    if (elementUp === ELEMENTS.BODY_RIGHT_DOWN) {
        return DIRECTIONS.DOWN;
    }
    if (elementUp === ELEMENTS.BODY_LEFT_DOWN) {
        return DIRECTIONS.DOWN;
    }
    if (elementDown === ELEMENTS.BODY_VERTICAL) {
        return DIRECTIONS.UP;
    }
    if (elementDown === ELEMENTS.BODY_RIGHT_UP) {
        return DIRECTIONS.UP;
    }
    if (elementDown === ELEMENTS.BODY_LEFT_UP) {
        return DIRECTIONS.UP;
    }
    // console.error('berserk mode direction last - ' + defaultDirection);
    return defaultDirection;
}

function isBerserkMode(board) {
    const currentState = board.getElementAt(board.getMyHead());
    return currentState === ELEMENTS.HEAD_EVIL;
}

function getAntStep(ant, board) {
    if (ant.done) {
        return ant;
    }
    if (ant.berserkMode) {//нужно для эвристики озверина
        if (ant.berserkLimitStep > 0) {
            ant.berserkLimitStep -= 1;
            if (ant.berserkLimitStep <= 0) {
                ant.berserkLimitStep = 0;
                ant.berserkMode = false;
            }
        }
    }
    let currentProfit = 0;

    if (isNewPoint(ant.currentPoint, ant.path)) {
        const estimateTable = getElementsEstimate(ant.berserkMode);
        const gameElement = board.getElementAt(ant.currentPoint)
        currentProfit = estimateTable[gameElement];
        if (gameElement === ELEMENTS.HEAD_EVIL) {
            ant.berserkMode = true;
            ant.berserkLimitStep += 10;
        }
    } else {
        if (isEmptyPoint(ant.currentPoint, ant.path, ant.antLength)) {
            currentProfit = 0
        } else {
            currentProfit = -5
        }
    }

    ant.profit += currentProfit;
    const isAntPathPositive = currentProfit >= 0 && !ant.donePositive;
    if (isAntPathPositive) {
        const isBestStartDirection = ant.countStep === 1;
        if (isBestStartDirection) {
            ant.profitPositive += currentProfit * 1.5;
        } else {
            ant.profitPositive += currentProfit;
        }
    } else {
        ant.donePositive = true;
    }
    ant.path.push(ant.currentPoint);


    const lastDirection = ant.directionsGo[ant.directionsGo.length - 1];
    const allowPotentialDirections = getAllowDirection(ant.currentPoint, lastDirection, board, ant);
    if (allowPotentialDirections.length > 0) {
        const choseDirection = getHeuristicRandomDirection(allowPotentialDirections);
        ant.directionsGo.push(choseDirection);
        ant.countStep += 1;
        ant.done = false;
        ant.currentPoint = getNextPoint(ant.currentPoint, choseDirection);
    } else {
        ant.done = true;
    }
    if (Number.isNaN(ant.profit)) {
        ant.done = true;
        debugger
    }
    return ant;
}

function isNewPoint(currentPoint, path) {
    for (const point of path) {
        if (point.equals(currentPoint)) {
            return false;
        }
    }
    return true;
}

function getElementsEstimate(berserkMode) {
    if (berserkMode) {
        return ELEMENTS_ESTIMATE_BERSERK;
    }
    return ELEMENTS_ESTIMATE;
}

function isEmptyPoint(currentPoint, path, antLength) {
    let start = path.length - antLength;
    if (path.length < antLength) {
        start = 0;
    }
    if (start < 0) {
        start = 0;
    }

    for (let i = start; i < path.length; i++) {
        if (currentPoint.equals(path[i])) {
            return false;
        }
    }
    return true;
}

function getAllowDirection(currentPoint, lastDirection, board, ant) {
    const sizeBorder = board.size;

    function isWell(element) {
        return element === ELEMENTS.WALL || element === ELEMENTS.START_FLOOR;
    }

    function isOutOfBoard(point) {
        return point.x > sizeBorder - 1 || point.y > sizeBorder - 1 || point.x < 1 || point.y < 1;
    }

    const elementRight = board.getElementAt(currentPoint.shiftRight(1));
    const elementLeft = board.getElementAt(currentPoint.shiftLeft(1));
    const elementUp = board.getElementAt(currentPoint.shiftTop(1));
    const elementDown = board.getElementAt(currentPoint.shiftBottom(1));

    const allowDirection = [];
    const disallowRight = isWell(elementRight) || lastDirection === DIRECTIONS.LEFT || isOutOfBoard(currentPoint.shiftRight(1));

    if (!disallowRight) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.RIGHT,
                'estimate': getEstimateForRandom(currentPoint.shiftRight(1), ant, board)
            });
    }
    const disallowLeft = isWell(elementLeft) || lastDirection === DIRECTIONS.RIGHT || isOutOfBoard(currentPoint.shiftLeft(1));

    if (!disallowLeft) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.LEFT,
                'estimate': getEstimateForRandom(currentPoint.shiftLeft(1), ant, board)
            });
    }
    const disallowUp = isWell(elementUp) || lastDirection === DIRECTIONS.DOWN || isOutOfBoard(currentPoint.shiftTop(1));

    if (!disallowUp) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.UP,
                'estimate': getEstimateForRandom(currentPoint.shiftTop(1), ant, board)
            });
    }
    const disallowDown = isWell(elementDown) || lastDirection === DIRECTIONS.UP || isOutOfBoard(currentPoint.shiftBottom(1));

    if (!disallowDown) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.DOWN,
                'estimate': getEstimateForRandom(currentPoint.shiftBottom(1), ant, board)
            });
    }
    return allowDirection;
}

function getEstimateForRandom(potentialPoint, ant, board) {
    let currentProfit = 0;
    if (isNewPoint(potentialPoint, ant.path)) {
        const estimateTable = getElementsEstimate(ant.berserkMode);
        const gameElement = board.getElementAt(potentialPoint)
        currentProfit = estimateTable[gameElement];
    } else {
        if (isEmptyPoint(potentialPoint, ant.path, ant.antLength)) {
            currentProfit = 0
        } else {
            currentProfit = -5
        }
    }
    return currentProfit;
}

function getDirectionsWithProbability(directions) {
    const total = 1;
    const partForOne = total / directions.length;
    const bestUpper = 0.1;
    const sortedDirections = directions.sort(function (a, b) {
        return b['estimate'] - a['estimate'];
    })
    for (let direction of sortedDirections) {
        direction['probability'] = partForOne;
    }
    if (sortedDirections[0]['estimate'] === sortedDirections[sortedDirections.length - 1]['estimate']) {
        return sortedDirections
    }
    if (sortedDirections.length === 2) {
        sortedDirections[0]['probability'] = partForOne + bestUpper;
        sortedDirections[1]['probability'] = partForOne - bestUpper;
    }
    if (sortedDirections.length === 3) {
        sortedDirections[0]['probability'] = partForOne + bestUpper;
        sortedDirections[1]['probability'] = partForOne;
        sortedDirections[2]['probability'] = partForOne - bestUpper;
    }
    return sortedDirections;
}

function getHeuristicRandomDirection(allowDirections) {
    if (allowDirections.length === 1) {
        return allowDirections[0]["direction"];
    }
    const sortedDirections = getDirectionsWithProbability(allowDirections);
    const rand = Math.random();
    let counterRand = 0;
    for (let direction of sortedDirections) {
        if (rand <= direction['probability'] + counterRand) {
            return direction["direction"];
        } else {
            counterRand += direction['probability']
        }
    }
    console.error('this code should not be used getHeuristicRandomDirection');
    return allowDirections[allowDirections.length - 1]["direction"];
}

function getNextPoint(currentPoint, direction) {
    if (direction === DIRECTIONS.LEFT) {
        return currentPoint.shiftLeft(1)
    }
    if (direction === DIRECTIONS.RIGHT) {
        return currentPoint.shiftRight(1)
    }
    if (direction === DIRECTIONS.UP) {
        return currentPoint.shiftTop(1)
    }
    if (direction === DIRECTIONS.DOWN) {
        return currentPoint.shiftBottom(1)
    }
}


function getBestAnt(antColony) {
    const sortedCollony = antColony.sort((a, b) => {
        return b['profitPositive'] + b['profit'] - a['profitPositive'] - a['profit']
    })
    for (let ant of sortedCollony) {
        if (!ant.done) {
            return ant;
        }
    }
    return sortedCollony[0];
}

/*Запасной алгоритм*/

function getEstimate(currentPoin, board) {
    function getElementsEstimate(board) {
        const currentState = board.getElementAt(board.getMyHead());
        if (currentState === ELEMENTS.HEAD_EVIL) {
            return ELEMENTS_ESTIMATE_BERSERK;
        }
        return ELEMENTS_ESTIMATE;
    }

    const estimateElements = getElementsEstimate(board);

    const currentPoint = board.getElementAt(currentPoin);
    const elementRight = board.getElementAt(currentPoin.shiftRight(1));
    const elementLeft = board.getElementAt(currentPoin.shiftLeft(1));
    const elementUp = board.getElementAt(currentPoin.shiftTop(1));
    const elementDown = board.getElementAt(currentPoin.shiftBottom(1));
    let estimate = 0;
    for (const el of [elementRight, elementLeft, elementUp, elementDown, currentPoint]) {
        if (isNaN(estimateElements[el])) {
            estimate += -100;
        } else {
            estimate += estimateElements[el];
        }
    }
    return estimate + 2 * estimateElements[currentPoint];
}

function getPrimaryElement(board) {
    let bestPoint = new Point(2, 2);
    let bestEstimate = 0;

    for (let x = 1; x < board.size; x++) {
        for (let y = 1; y < board.size; y++) {
            let curEstimate = getEstimate(new Point(x, y), board);
            if (curEstimate >= bestEstimate) {
                bestEstimate = curEstimate;
                bestPoint = new Point(x, y);
            }
        }
    }
    console.log("best Point " + bestPoint);
    return bestPoint;
    return board.findFirstElement(ELEMENTS.FURY_PILL);
}


function estimateRightDeep(board, shift) {
    let estimate = 0
    for (let i = 1; i <= shift; i++) {
        // console.log("step estimateRightDeep ");
        // console.log(i);
        // console.log(getEstimate(board.getMyHead().shiftRight(i), board));
        if (isNaN(getEstimate(board.getMyHead().shiftRight(i), board))) {
            estimate += -100;
        } else {
            estimate += (shift - i) * getEstimate(board.getMyHead().shiftRight(i), board);
        }

    }
    return estimate;
}

function estimateLeftDeep(board, shift) {
    let estimate = 0
    for (let i = 1; i <= shift; i++) {
        if (isNaN(getEstimate(board.getMyHead().shiftLeft(i), board))) {
            estimate += -100
        } else {
            estimate += (shift - i) * getEstimate(board.getMyHead().shiftLeft(i), board);
        }
    }
    return estimate;
}

function estimateUpDeep(board, shift) {
    let estimate = 0
    for (let i = 1; i <= shift; i++) {
        if (isNaN(getEstimate(board.getMyHead().shiftTop(i), board))) {
            estimate += -100
        } else {
            estimate += (shift - i) * getEstimate(board.getMyHead().shiftTop(i), board);
        }
    }
    return estimate;
}

function estimateDownDeep(board, shift) {
    let estimate = 0
    for (let i = 1; i <= shift; i++) {
        if (isNaN(getEstimate(board.getMyHead().shiftBottom(i), board))) {
            estimate += -100
        } else {
            estimate += (shift - i) * getEstimate(board.getMyHead().shiftBottom(i), board);
        }
    }
    return estimate;
}

function canRight(board) {
    const currentPoin = board.getMyHead();
    const curDirection = board.getElementAt(currentPoin);
    return curDirection !== ELEMENTS.HEAD_LEFT;
}

function canLeft(board) {
    const currentPoin = board.getMyHead();
    const curDirection = board.getElementAt(currentPoin);
    return curDirection !== ELEMENTS.HEAD_RIGHT;
}

function canUp(board) {
    const currentPoin = board.getMyHead();
    const curDirection = board.getElementAt(currentPoin);
    return curDirection !== ELEMENTS.HEAD_DOWN;
}


function game(board) {
    let deap = 3;
    if (Math.random() > 0.8) {
        deap = 2;
    }
    if (Math.random() < 0.1) {
        deap = 1;
    }

    try {
        const isValidBoard = board.getMyHead();
    } catch (e) {
        console.error(e);
        return new Action(DIRECTIONS.DOWN, isActing);
    }

    let elementRight = estimateRightDeep(board, deap);
    let elementLeft = estimateLeftDeep(board, deap);
    let elementUp = estimateUpDeep(board, deap);
    let elementDown = estimateDownDeep(board, deap);

    const primaryPoint = getPrimaryElement(board);
    const currentPosition = board.getMyHead();
    const bonusPoint = 2;
    if (primaryPoint) {
        if (currentPosition.x < primaryPoint.x) {
            elementRight += bonusPoint;
            console.log("go Right")
        }
        if (currentPosition.x > primaryPoint.x) {
            elementLeft += bonusPoint;
        }
        if (currentPosition.y < primaryPoint.y) {
            elementDown += bonusPoint;
        }
        if (currentPosition.y > primaryPoint.y) {
            elementUp += bonusPoint;
            console.log("go elementDown")
        }
    }


    console.log({
        elementRight,
        elementLeft,
        elementUp,
        elementDown
    });
    const isActing = false;
    if (elementRight >= Math.max(elementLeft, elementUp, elementDown)) {
        if (canRight(board)) {
            return new Action(DIRECTIONS.RIGHT, isActing);
        } else {
            if (elementLeft >= Math.max(elementUp, elementDown)) {
                return new Action(DIRECTIONS.LEFT, isActing);
            }
            if (elementUp >= Math.max(elementLeft, elementDown)) {
                return new Action(DIRECTIONS.UP, isActing);
            }
            if (elementDown >= Math.max(elementLeft, elementUp)) {
                return new Action(DIRECTIONS.DOWN, isActing);
            }
        }
    }
    if (elementLeft >= Math.max(elementRight, elementUp, elementDown)) {
        if (canLeft(board)) {
            return new Action(DIRECTIONS.LEFT, isActing);
        } else {
            if (elementRight >= Math.max(elementUp, elementDown)) {
                return new Action(DIRECTIONS.RIGHT, isActing);
            }
            if (elementUp >= Math.max(elementRight, elementDown)) {
                return new Action(DIRECTIONS.UP, isActing);
            }
            if (elementDown >= Math.max(elementRight, elementUp)) {
                return new Action(DIRECTIONS.DOWN, isActing);
            }
        }
    }
    if (elementUp >= Math.max(elementLeft, elementRight, elementDown)) {
        if (canUp(board)) {
            return new Action(DIRECTIONS.UP, isActing);
        }
    }
    if (elementDown >= Math.max(elementLeft, elementUp, elementRight)) {
        return new Action(DIRECTIONS.DOWN, isActing);
    }


    return new Action(DIRECTIONS.DOWN, isActing);

}


function checkPart(board, bestAnt) {
    const wells = board.getWalls();
    for (const path of bestAnt.path) {
        const el = board.getElementAt(path);
        for (const well of wells) {
            const elw = board.getElementAt(well);
            if (path.equals(well)) {
                const elw = board.getElementAt(well);
            }
        }
    }
}