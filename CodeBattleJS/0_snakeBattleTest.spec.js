const a = 'board=☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼      ○                         ☼☼☼         ○                      ☼☼#   ☼☼☼   ☼☼☼    ☼#   ☼☼☼   ☼☼☼  ☼☼☼   ☼       ☼         ☼   $   ☼  ☼☼☼  ®☼       ☼         ☼ ● ● ● ☼  ☼☼☼                                ☼☼☼     ●                     ●    ☼☼☼             ®                  ☼☼#   ☼ ● ●   ☼    ☼#○  ☼       ☼  ☼☼☼   ☼       ☼         ☼       ☼  ☼☼☼   ☼☼☼   ☼☼☼         ☼☼☼   ☼☼☼  ☼☼☼                   ®      ╔═══♥ ☼☼☼ ® ☼☼☼   ☼☼☼         ☼☼☼  ║☼☼☼  ☼☼☼   ☼     ® ☼    ®    ☼    ║  ☼  ☼☼#  ®☼ ● ● ● ☼    ☼#   ☼ ● ●║● ☼  ☼☼☼ ○       ○            ╔══╗║     ☼☼☼     ●   ●            ║●®╚╝●    ☼☼☼    ®   ♣             ╚╕        ☼☼☼   ☼ ● ●│● ☼         ☼ ● ●   ☼  ☼☼☼   ☼    │  ☼         ☼       ☼  ☼☼#   ☼☼☼  │☼☼☼    ☼#  ®☼☼☼   ☼☼☼  ☼☼☼   $    │ ○                     ☼☼☼   ☼☼☼  │☼☼☼         ☼☼☼   ☼☼☼  ☼☼☼   ☼    └─┐☼         ☼   ○   ☼  ☼☼☼   ☼      │☼         ☼ ● ● ● ☼  ☼☼☼      ┌───┘             ○○○     ☼☼#     ●│         ☼#     ●○$○●    ☼☼☼ ┌────┘                 ○○○ ®   ☼☼☼ │ ☼ ●     ☼         ☼ ● ● ● ☼  ☼☼☼ ¤ ☼       ☼  ®      ☼    ®  ☼  ☼☼☼   ☼☼☼   ☼☼☼         ☼☼☼   ☼☼☼  ☼☼☼                           ®    ☼☼☼                    ®       ○   ☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼';

const ACT_COMMAND_PREFIX = "ACT,";

class Action {
    constructor(direction, act) {
        this.direction = direction;
        this.act = act;
    }

    toString() {
        const command = this.act ? ACT_COMMAND_PREFIX : "";
        return command + this.direction;
    }
}

class Board {
    constructor() {
        this._board = "";
        this._boardMapped = "";
    }

    get size() {
        return Math.floor(Math.sqrt(this._board.length));
    }

    get size_length() {
        return this._board.length;
    }

    findElement(elementType) {
        const foundPoints = this._getMappedBoard()
            .filter(element => element.type === elementType)
            .map(element => element.coordinates);

        return foundPoints[0] || null;
    }

    findFirstElement(...elementTypes) {
        const element = this._getMappedBoard().find(element => {
            return elementTypes.includes(element.type);
        });

        if (!element) {
            return null;
        }

        return element.coordinates;
    }

    findAllElements(...elementTypes) {
        return this._getMappedBoard().reduce((points, element, index) => {
            if (elementTypes.includes(element.type)) {
                points.push(element.coordinates);
            }

            return points;
        }, []);
    }

    getElementAt(point) {
        const element = this._getMappedBoard().find(element => {
            return element.coordinates.equals(point);
        });

        if (!element) {
            return null;
        }

        return element.type;
    }

    hasElementAt(elementType, point) {
        return this.getElementAt(point) === elementType;
    }

    amIEvil() {
        return this.getMyHead() === ELEMENTS.HEAD_EVIL;
    }

    amIFlying() {
        return this.getMyHead() === ELEMENTS.HEAD_FLY;
    }

    getWalls() {
        return this.findAllElements(ELEMENTS.WALL);
    }

    getStones() {
        return this.findAllElements(ELEMENTS.STONE);
    }

    getApples() {
        return this.findAllElements(ELEMENTS.APPLE);
    }

    getGold() {
        return this.findAllElements(ELEMENTS.FURY_PILL);
    }

    getFuryPills() {
        return this.findAllElements(ELEMENTS.FURY_PILL);
    }

    getStartPoints() {
        return this.findAllElements(ELEMENTS.START_FLOOR);
    }

    getFlyingPills() {
        return this.findAllElements(ELEMENTS.FLYING_PILL);
    }

    getMyHead() {
        const headElementTypes = [
            "HEAD_DEAD",
            "HEAD_DOWN",
            "HEAD_UP",
            "HEAD_LEFT",
            "HEAD_RIGHT",
            "HEAD_EVIL",
            "HEAD_FLY",
            "HEAD_SLEEP"
        ].map(elementName => ELEMENTS[elementName]);

        return this.findFirstElement(...headElementTypes);
    }

    getBarriers() {
        const elementTypes = [
            "WALL",
            "START_FLOOR",
            "ENEMY_HEAD_SLEEP",
            "ENEMY_TAIL_INACTIVE",
            "TAIL_INACTIVE",
            "STONE"
        ].map(elementName => ELEMENTS[elementName]);

        return this.findAllElements(...elementTypes);
    }

    isBarrierAt(point) {
        return !!this.getBarriers().find(barrierPoint =>
            barrierPoint.equals(point)
        );
    }

    update(raw) {
        this._board = raw.replace("board=", "");
        this._boardMapped = this._board.split("").map((element, index) => {
            return {type: element, coordinates: this.getPointByShift(index)};
        });
    }

    _getMappedBoard() {
        return this._boardMapped;
    }

    getPointByShift(shift) {
        return new Point(shift % this.size, Math.floor(shift / this.size));
    }

    toString() {
        const lineRegExp = new RegExp(`(.{${this.size}})`, "g");
        return this._board.replace(lineRegExp, "$1\n");
    }

    getLengthMyShape() {
        const headElementTypes = [
            "HEAD_DEAD",
            "HEAD_DOWN",
            "HEAD_UP",
            "HEAD_LEFT",
            "HEAD_RIGHT",
            "HEAD_EVIL",
            "HEAD_FLY",
            "HEAD_SLEEP",

            "TAIL_END_DOWN",
            "TAIL_END_LEFT",
            "TAIL_END_UP",
            "TAIL_END_RIGHT",
            "TAIL_INACTIVE",

            "BODY_HORIZONTAL",
            "BODY_VERTICAL",
            "BODY_LEFT_DOWN",
            "BODY_LEFT_UP",
            "BODY_RIGHT_DOWN",
            "BODY_RIGHT_UP",
        ].map(elementName => ELEMENTS[elementName]);

        return this.findAllElements(...headElementTypes).length;

    }
}

const DIRECTIONS = {
    UP: "UP",
    RIGHT: "RIGHT",
    DOWN: "DOWN",
    LEFT: "LEFT",
    STOP: "STOP"
};
const ELEMENTS = {
    NONE: " ", // пустое место
    WALL: "☼", // а это стенка
    START_FLOOR: "#", // место старта змей
    OTHER: "?", // этого ты никогда не увидишь :)

    APPLE: "○", // яблоки надо кушать от них становишься длинее
    STONE: "●", // а это кушать не стоит - от этого укорачиваешься
    FLYING_PILL: "©", // таблетка полета - дает суперсилы
    FURY_PILL: "®", // таблетка ярости - дает суперсилы
    GOLD: "$", // золото - просто очки

    // голова твоей змеи в разных состояниях и направлениях
    HEAD_DOWN: "▼",
    HEAD_LEFT: "◄",
    HEAD_RIGHT: "►",
    HEAD_UP: "▲",
    HEAD_DEAD: "☻", // этот раунд ты проиграл
    HEAD_EVIL: "♥", // ты скушал таблетку ярости
    HEAD_FLY: "♠", // ты скушал таблетку полета
    HEAD_SLEEP: "&", // твоя змейка ожидает начала раунда

    // хвост твоей змейки
    TAIL_END_DOWN: "╙",
    TAIL_END_LEFT: "╘",
    TAIL_END_UP: "╓",
    TAIL_END_RIGHT: "╕",
    TAIL_INACTIVE: "~",

    // туловище твоей змейки
    BODY_HORIZONTAL: "═",
    BODY_VERTICAL: "║",
    BODY_LEFT_DOWN: "╗",
    BODY_LEFT_UP: "╝",
    BODY_RIGHT_DOWN: "╔",
    BODY_RIGHT_UP: "╚",

    // змейки противников
    ENEMY_HEAD_DOWN: "˅",
    ENEMY_HEAD_LEFT: "<",
    ENEMY_HEAD_RIGHT: ">",
    ENEMY_HEAD_UP: "˄",
    ENEMY_HEAD_DEAD: "☺", // этот раунд противник проиграл
    ENEMY_HEAD_EVIL: "♣", // противник скушал таблетку ярости
    ENEMY_HEAD_FLY: "♦", // противник скушал таблетку полета
    ENEMY_HEAD_SLEEP: "ø", // змейка противника ожидает начала раунда

    // хвосты змеек противников
    ENEMY_TAIL_END_DOWN: "¤",
    ENEMY_TAIL_END_LEFT: "×",
    ENEMY_TAIL_END_UP: "æ",
    ENEMY_TAIL_END_RIGHT: "ö",
    ENEMY_TAIL_INACTIVE: "*",

    // туловище змеек противников
    ENEMY_BODY_HORIZONTAL: "─",
    ENEMY_BODY_VERTICAL: "│",
    ENEMY_BODY_LEFT_DOWN: "┐",
    ENEMY_BODY_LEFT_UP: "┘",
    ENEMY_BODY_RIGHT_DOWN: "┌",
    ENEMY_BODY_RIGHT_UP: "└"
};

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    isOutOfBoard(boardSize) {
        return (
            this.x >= boardSize || this.y >= boardSize || this.x < 0 || this.y < 0
        );
    }

    equals(point) {
        return this.x === point.x && this.y === point.y;
    }

    notEquals() {
        return !this.equals(point);
    }

    shiftTop(delta = 1) {
        return new Point(this.x, this.y - delta);
    }

    shiftLeft(delta = 1) {
        return new Point(this.x - delta, this.y);
    }

    shiftRight(delta = 1) {
        return new Point(this.x + delta, this.y);
    }

    shiftBottom(delta = 1) {
        return new Point(this.x, this.y + delta);
    }

    toString() {
        return `[${this.x},${this.y}`;
    }
}


const ELEMENTS_ESTIMATE = {
    [ELEMENTS.NONE]: 1, // пустое место
    [ELEMENTS.WALL]: -100, // а это стенка
    [ELEMENTS.START_FLOOR]: -100, // место старта змей
    [ELEMENTS.OTHER]: -100, // этого ты никогда не увидишь :)

    [ELEMENTS.APPLE]: 15, // яблоки надо кушать от них становишься длинее
    [ELEMENTS.STONE]: -19, // а это кушать не стоит - от этого укорачиваешься
    [ELEMENTS.FLYING_PILL]: 15, // таблетка полета - дает суперсилы
    [ELEMENTS.FURY_PILL]: 40, // таблетка ярости - дает суперсилы
    [ELEMENTS.GOLD]: 10, // золото - просто очки

    // голова твоей змеи в разных состояниях и направлениях
    [ELEMENTS.HEAD_DOWN]: 0,
    [ELEMENTS.HEAD_LEFT]: 0,
    [ELEMENTS.HEAD_RIGHT]: 0,
    [ELEMENTS.HEAD_UP]: 0,
    [ELEMENTS.HEAD_DEAD]: 0, // этот раунд ты проиграл
    [ELEMENTS.HEAD_EVIL]: 0, // ты скушал таблетку ярости
    [ELEMENTS.HEAD_FLY]: 0, // ты скушал таблетку полета
    [ELEMENTS.HEAD_SLEEP]: 0, // твоя змейка ожидает начала раунда

    // хвост твоей змейки
    [ELEMENTS.TAIL_END_DOWN]: 0,
    [ELEMENTS.TAIL_END_LEFT]: 0,
    [ELEMENTS.TAIL_END_UP]: 0,
    [ELEMENTS.TAIL_END_RIGHT]: 0,
    [ELEMENTS.TAIL_INACTIVE]: 0,

    // туловище твоей змейки
    [ELEMENTS.BODY_HORIZONTAL]: -20,
    [ELEMENTS.BODY_VERTICAL]: -20,
    [ELEMENTS.BODY_LEFT_DOWN]: -20,
    [ELEMENTS.BODY_LEFT_UP]: -20,
    [ELEMENTS.BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.BODY_RIGHT_UP]: -20,

    // змейки противников
    [ELEMENTS.ENEMY_HEAD_DOWN]: -10,
    [ELEMENTS.ENEMY_HEAD_LEFT]: -10,
    [ELEMENTS.ENEMY_HEAD_RIGHT]: -10,
    [ELEMENTS.ENEMY_HEAD_UP]: -10,
    [ELEMENTS.ENEMY_HEAD_DEAD]: 0, // этот раунд противник проиграл
    [ELEMENTS.ENEMY_HEAD_EVIL]: 0, // противник скушал таблетку ярости
    [ELEMENTS.ENEMY_HEAD_FLY]: 0, // противник скушал таблетку полета
    [ELEMENTS.ENEMY_HEAD_SLEEP]: 0, // змейка противника ожидает начала раунда

    // хвосты змеек противников
    [ELEMENTS.ENEMY_TAIL_END_DOWN]: -5,
    [ELEMENTS.ENEMY_TAIL_END_LEFT]: -5,
    [ELEMENTS.ENEMY_TAIL_END_UP]: -5,
    [ELEMENTS.ENEMY_TAIL_END_RIGHT]: -5,
    [ELEMENTS.ENEMY_TAIL_INACTIVE]: -5,

    // туловище змеек противников
    [ELEMENTS.ENEMY_BODY_HORIZONTAL]: -20,
    [ELEMENTS.ENEMY_BODY_VERTICAL]: -20,
    [ELEMENTS.ENEMY_BODY_LEFT_DOWN]: -20,
    [ELEMENTS.ENEMY_BODY_LEFT_UP]: -20,
    [ELEMENTS.ENEMY_BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.ENEMY_BODY_RIGHT_UP]: -20
};


const ELEMENTS_ESTIMATE_BERSERK = {
    [ELEMENTS.NONE]: 1, // пустое место
    [ELEMENTS.WALL]: -100, // а это стенка
    [ELEMENTS.START_FLOOR]: -100, // место старта змей
    [ELEMENTS.OTHER]: -100, // этого ты никогда не увидишь :)

    [ELEMENTS.APPLE]: 15, // яблоки надо кушать от них становишься длинее
    [ELEMENTS.STONE]: 15, // а это кушать не стоит - от этого укорачиваешься
    [ELEMENTS.FLYING_PILL]: 15, // таблетка полета - дает суперсилы
    [ELEMENTS.FURY_PILL]: 35, // таблетка ярости - дает суперсилы
    [ELEMENTS.GOLD]: 10, // золото - просто очки

    // голова твоей змеи в разных состояниях и направлениях
    [ELEMENTS.HEAD_DOWN]: 0,
    [ELEMENTS.HEAD_LEFT]: 0,
    [ELEMENTS.HEAD_RIGHT]: 0,
    [ELEMENTS.HEAD_UP]: 0,
    [ELEMENTS.HEAD_DEAD]: 0, // этот раунд ты проиграл
    [ELEMENTS.HEAD_EVIL]: 0, // ты скушал таблетку ярости
    [ELEMENTS.HEAD_FLY]: 0, // ты скушал таблетку полета
    [ELEMENTS.HEAD_SLEEP]: 0, // твоя змейка ожидает начала раунда

    // хвост твоей змейки
    [ELEMENTS.TAIL_END_DOWN]: 0,
    [ELEMENTS.TAIL_END_LEFT]: 0,
    [ELEMENTS.TAIL_END_UP]: 0,
    [ELEMENTS.TAIL_END_RIGHT]: 0,
    [ELEMENTS.TAIL_INACTIVE]: 0,

    // туловище твоей змейки
    [ELEMENTS.BODY_HORIZONTAL]: -20,
    [ELEMENTS.BODY_VERTICAL]: -20,
    [ELEMENTS.BODY_LEFT_DOWN]: -20,
    [ELEMENTS.BODY_LEFT_UP]: -20,
    [ELEMENTS.BODY_RIGHT_DOWN]: -20,
    [ELEMENTS.BODY_RIGHT_UP]: -20,

    // змейки противников
    [ELEMENTS.ENEMY_HEAD_DOWN]: 26,
    [ELEMENTS.ENEMY_HEAD_LEFT]: 26,
    [ELEMENTS.ENEMY_HEAD_RIGHT]: 26,
    [ELEMENTS.ENEMY_HEAD_UP]: 26,
    [ELEMENTS.ENEMY_HEAD_DEAD]: 0, // этот раунд противник проиграл
    [ELEMENTS.ENEMY_HEAD_EVIL]: 0, // противник скушал таблетку ярости
    [ELEMENTS.ENEMY_HEAD_FLY]: 0, // противник скушал таблетку полета
    [ELEMENTS.ENEMY_HEAD_SLEEP]: 0, // змейка противника ожидает начала раунда

    // хвосты змеек противников
    [ELEMENTS.ENEMY_TAIL_END_DOWN]: 1,
    [ELEMENTS.ENEMY_TAIL_END_LEFT]: 1,
    [ELEMENTS.ENEMY_TAIL_END_UP]: 1,
    [ELEMENTS.ENEMY_TAIL_END_RIGHT]: 1,
    [ELEMENTS.ENEMY_TAIL_INACTIVE]: 1,

    // туловище змеек противников
    [ELEMENTS.ENEMY_BODY_HORIZONTAL]: 25,
    [ELEMENTS.ENEMY_BODY_VERTICAL]: 25,
    [ELEMENTS.ENEMY_BODY_LEFT_DOWN]: 25,
    [ELEMENTS.ENEMY_BODY_LEFT_UP]: 25,
    [ELEMENTS.ENEMY_BODY_RIGHT_DOWN]: 25,
    [ELEMENTS.ENEMY_BODY_RIGHT_UP]: 25
};
describe('Index page', () => {
    const accuracy = 5;
    const board = new Board();
    board.update(a);

    const ant = {
        "antLength": 18,
        "berserkLimitStep": 0,
        "berserkMode": true,
        "countStep": 0,
        "currentPoint": new Point(32, 12),
        "directionsGo": [
            DIRECTIONS.RIGHT
        ],
        "done": false,
        "donePositive": false,
        "path": [],
        "profit": 0,
        "profitPositive": 0
    };

    it('Проверка правильности элементов', () => {
        const problemPoint = new Point(1, 1);
        const el = board.getElementAt(new Point(3, 3));
        for (let i = 0; i < board.size; i++) {
            let el = board.getElementAt(new Point(i, 0));
            expect(el).toEqual(ELEMENTS.WALL, accuracy);
        }
        for (let i = 0; i < board.size; i++) {
            let el = board.getElementAt(new Point(0, i));
            expect(el).toEqual(ELEMENTS.WALL, accuracy);
        }
        expect(board.getElementAt(new Point(1, 3))).toEqual(ELEMENTS.START_FLOOR, accuracy);
        expect(board.getElementAt(new Point(7, 7))).toEqual(ELEMENTS.STONE, accuracy);
        expect(board.getElementAt(new Point(8, 1))).toEqual(ELEMENTS.APPLE, accuracy);
    });

    it('Проверка правильности генерации колонии', () => {
        const lastDirection = DIRECTIONS.RIGHT;
        const ants = generateAntColony(board, 2, lastDirection);
        expect(ants[0]).toEqual(ants[1], accuracy);
        expect(ants[0]).toEqual({
            "antLength": 18,
            "berserkLimitStep": 0,
            "berserkMode": true,
            "countStep": 0,
            "currentPoint": {
                "x": 32,
                "y": 12
            },
            "directionsGo": [
                DIRECTIONS.RIGHT
            ],
            "done": false,
            "donePositive": false,
            "path": [],
            "profit": 0,
            "profitPositive": 0
        }, accuracy);

    });

    it('Проверка правильности текущего направления', () => {
        const lastDirection = DIRECTIONS.RIGHT;
        const adirection = getLastDirection(board);
        expect(adirection).toEqual('RIGHT', accuracy);
    });
    it('Проверка правильности выбора направлений', () => {
        const lastDirection = DIRECTIONS.RIGHT;
        const directions = getAllowDirection(new Point(32, 12), lastDirection, board, ant);
        expect(directions).toEqual([
            {
                "direction": "RIGHT",
                "estimate": 1
            },
            {
                "direction": "UP",
                "estimate": 1
            },
            {
                "direction": "DOWN",
                "estimate": 1
            }
        ], accuracy);
        expect(getAllowDirection(new Point(0, 0), lastDirection, board, ant)
        ).toEqual([], accuracy);
        expect(getAllowDirection(new Point(2, 1), lastDirection, board, ant)
        ).toEqual([
            {
                "direction": "RIGHT",
                "estimate": 1
            },
            {
                "direction": "DOWN",
                "estimate": 1
            }
        ], accuracy);
        expect(getAllowDirection(new Point(2, 1), DIRECTIONS.LEFT, board, ant)
        ).toEqual([
            {
                "direction": "DOWN",
                "estimate": 1
            }
        ], accuracy);
        expect(getAllowDirection(new Point(8, 1), DIRECTIONS.DOWN, board, ant)
        ).toEqual([
            {
                "direction": "RIGHT",
                "estimate": 1
            },
            {
                "direction": "LEFT",
                "estimate": 1
            },
            {
                "direction": "DOWN",
                "estimate": 1
            }
        ], accuracy);
    });

    it('Проверка первого шага колонии', () => {
        let antStep = getAntStep({
            "antLength": 18,
            "berserkLimitStep": 0,
            "berserkMode": true,
            "countStep": 0,
            "currentPoint": new Point(32, 12),
            "directionsGo": [
                DIRECTIONS.RIGHT
            ],
            "done": false,
            "donePositive": false,
            "path": [],
            "profit": 0,
            "profitPositive": 0
        }, board);
        for (let i = 0; i < 8; i++) {
            antStep = getAntStep(antStep, board);
        }
        expect(antStep).toEqual({}, accuracy);


    });

    it('Проверка предворительной оценки направления', () => {
        let estimate = getEstimateForRandom(new Point(33, 12),
            {
                "antLength": 18,
                "berserkLimitStep": 0,
                "berserkMode": true,
                "countStep": 0,
                "currentPoint": new Point(32, 12),
                "directionsGo": [
                    DIRECTIONS.RIGHT
                ],
                "done": false,
                "donePositive": false,
                "path": [],
                "profit": 0,
                "profitPositive": 0
            }, board);
        expect(estimate).toEqual(1, accuracy);

    });

    it('Проверка первого а были ли мы в этой точке', () => {
        const path = [
            new Point(1, 1),
            new Point(2, 2),
            new Point(3, 3),
            new Point(4, 4)
        ]
        expect(isEmptyPoint(new Point(1, 1), path, 4)).toEqual(false, accuracy);
        expect(isEmptyPoint(new Point(1, 1), path, 3)).toEqual(true, accuracy);
        expect(isEmptyPoint(new Point(2, 2), path, 3)).toEqual(false, accuracy);
        expect(isEmptyPoint(new Point(8, 8), path, 3)).toEqual(true, accuracy);


    });


    it('Проверка игры', () => {
        const ansver = gameAnt(board);
        expect(ansver).toEqual(ansver, accuracy);


    });

    it('Проверка распределения вероятностей', () => {
        const directions = [
            {
                'direction': DIRECTIONS.LEFT,
                'estimate': -5
            },
            {
                'direction': DIRECTIONS.RIGHT,
                'estimate': 10
            },
            {
                'direction': DIRECTIONS.UP,
                'estimate': 1
            },
        ];
        expect(getDirectionsWithProbability(directions)).toEqual([
            {
                "direction": "LEFT",
                "estimate": -5,
                "probability": 0.4833333333333333
            },
            {
                "direction": "UP",
                "estimate": 1,
                "probability": 0.3333333333333333
            },
            {
                "direction": "RIGHT",
                "estimate": 10,
                "probability": 0.18333333333333332
            }
        ], accuracy);
        expect(getDirectionsWithProbability([
            {
                'direction': DIRECTIONS.LEFT,
                'estimate': -5
            },
            {
                'direction': DIRECTIONS.RIGHT,
                'estimate': 10
            },
        ])).toEqual([
            {
                "direction": "LEFT",
                "estimate": -5,
                "probability": 0.65
            },
            {
                "direction": "RIGHT",
                "estimate": 10,
                "probability": 0.35
            }
        ], accuracy);
        expect(getHeuristicRandomDirection([
            {
                'direction': DIRECTIONS.LEFT,
                'estimate': -5
            },
            {
                'direction': DIRECTIONS.RIGHT,
                'estimate': 10
            },
        ])).toEqual(DIRECTIONS.LEFT, accuracy);


    });
});

function checkWells(board, bestAnt) {
    const wells = board.getWalls();
    for (const path of bestAnt.path) {
        for (const well of wells) {
            if (path.equals(well)) {
                debugger;
            }
        }
    }
}

let lastDirection = DIRECTIONS.RIGHT;

function gameAnt(board) {
    const dateStart = performance.now();
    const antColony = generateAntColony(board, 100, lastDirection);
    const maxSteps = 25;
    for (let i = 0; i < maxSteps; i++) {
        for (let j = 0; j < antColony.length; j++) {
            antColony[j] = getAntStep(antColony[j], board);
            // if (performance.now() - dateStart > 800) {
            //     break;
            // }
        }
    }

    const bestAnt = getBestAnt(antColony);

    const sortedAntColony = antColony.sort(function (a, b) {
        return b['profit'] - a['profit'];
    })
    const sortedAntColonyPositive = antColony.sort(function (a, b) {
        return b['profitPositive'] - a['profitPositive'];
    })

    const isActing = false;
    console.log(bestAnt);
    const dateEnd = performance.now();
    console.log("Call to gameAnt took " + (dateEnd - dateStart) + " milliseconds.")
    console.log(bestAnt.profit)
    lastDirection = bestAnt.directionsGo[1];
    return new Action(bestAnt.directionsGo[1], isActing);
}

function generateAntColony(board, countAnt, defaultDirection) {
    const antColony = [];

    for (let i = 0; i < countAnt; i++) {
        let ant = {
            path: [],
            directionsGo: [getLastDirection(board, defaultDirection)],
            currentPoint: board.getMyHead(),
            profit: 0,
            profitPositive: 0,
            done: false,
            donePositive: false,
            countStep: 0,
            berserkMode: isBerserkMode(board),
            berserkLimitStep: 0,
            antLength: getAntLength(board)
        }
        antColony.push(ant);
    }
    return antColony;

    function getAntLength(board) {
        if (getAntLength.board !== board.toString()) {
            getAntLength.board = board.toString();
            getAntLength.instance = board.getLengthMyShape();
        }
        return getAntLength.instance;
    }
}

function getLastDirection(board, defaultDirection) {
    const currentElement = board.getElementAt(board.getMyHead());
    if (currentElement === ELEMENTS.HEAD_RIGHT) {
        return DIRECTIONS.RIGHT;
    }
    if (currentElement === ELEMENTS.HEAD_LEFT) {
        return DIRECTIONS.LEFT;
    }
    if (currentElement === ELEMENTS.HEAD_UP) {
        return DIRECTIONS.UP;
    }
    if (currentElement === ELEMENTS.HEAD_DOWN) {
        return DIRECTIONS.DOWN;
    }
    const currentPoint = board.getMyHead();
    const elementRight = board.getElementAt(currentPoint.shiftRight(1));
    const elementLeft = board.getElementAt(currentPoint.shiftLeft(1));
    const elementUp = board.getElementAt(currentPoint.shiftTop(1));
    const elementDown = board.getElementAt(currentPoint.shiftBottom(1));
    // туловище твоей змейки
    // BODY_HORIZONTAL: "═",
    //     BODY_VERTICAL: "║",
    //     BODY_LEFT_DOWN: "╗",
    //     BODY_LEFT_UP: "╝",
    //     BODY_RIGHT_DOWN: "╔",
    //     BODY_RIGHT_UP: "╚",
    if (elementRight === ELEMENTS.BODY_HORIZONTAL) {
        return DIRECTIONS.LEFT;
    }
    if (elementLeft === ELEMENTS.BODY_HORIZONTAL) {
        return DIRECTIONS.RIGHT;
    }
    if (elementUp === ELEMENTS.BODY_VERTICAL) {
        return DIRECTIONS.DOWN;
    }
    if (elementUp === ELEMENTS.BODY_RIGHT_DOWN) {
        return DIRECTIONS.DOWN;
    }
    if (elementUp === ELEMENTS.BODY_LEFT_DOWN) {
        return DIRECTIONS.DOWN;
    }
    if (elementDown === ELEMENTS.BODY_VERTICAL) {
        return DIRECTIONS.UP;
    }
    if (elementDown === ELEMENTS.BODY_RIGHT_UP) {
        return DIRECTIONS.UP;
    }
    if (elementDown === ELEMENTS.BODY_LEFT_UP) {
        return DIRECTIONS.UP;
    }
    console.error('berserk mode direction last - ' + defaultDirection);
    return defaultDirection;
}

function isBerserkMode(board) {
    const currentState = board.getElementAt(board.getMyHead());
    return currentState === ELEMENTS.HEAD_EVIL;
}

function getAntStep(ant, board) {
    if (ant.done) {
        return ant;
    }
    if (ant.berserkMode) {//нужно для эвристики озверина
        if (ant.berserkLimitStep > 0) {
            ant.berserkLimitStep -= 1;
            if (ant.berserkLimitStep <= 0) {
                ant.berserkLimitStep = 0;
                ant.berserkMode = false;
            }
        }
    }
    let currentProfit = 0;

    if (isNewPoint(ant.currentPoint, ant.path)) {
        const estimateTable = getElementsEstimate(ant.berserkMode);
        const gameElement = board.getElementAt(ant.currentPoint)
        currentProfit = estimateTable[gameElement];
        if (gameElement === ELEMENTS.HEAD_EVIL) {
            ant.berserkMode = true;
            ant.berserkLimitStep += 10;
        }
    } else {
        if (isEmptyPoint(ant.currentPoint, ant.path, ant.antLength)) {
            currentProfit = 0
        } else {
            currentProfit = -5
        }
    }

    ant.profit += currentProfit;
    const isAntPathPositive = currentProfit >= 0 && !ant.donePositive;
    if (isAntPathPositive) {
        ant.profitPositive += currentProfit;
    } else {
        ant.donePositive = true;
    }
    ant.path.push(ant.currentPoint);


    const lastDirection = ant.directionsGo[ant.directionsGo.length - 1];
    const allowPotentialDirections = getAllowDirection(ant.currentPoint, lastDirection, board, ant);
    if (allowPotentialDirections.length > 0) {
        const choseDirection = getHeuristicRandomDirection(allowPotentialDirections);
        ant.directionsGo.push(choseDirection);
        ant.countStep += 1;
        ant.done = false;
        ant.currentPoint = getNextPoint(ant.currentPoint, choseDirection);
    } else {
        ant.done = true;
    }
    if (Number.isNaN(ant.profit)) {
        ant.done = true;
        debugger
    }
    return ant;
}

function isNewPoint(currentPoint, path) {
    for (const point of path) {
        if (point.equals(currentPoint)) {
            return false;
        }
    }
    return true;
}

function getElementsEstimate(berserkMode) {
    if (berserkMode) {
        return ELEMENTS_ESTIMATE_BERSERK;
    }
    return ELEMENTS_ESTIMATE;
}

function isEmptyPoint(currentPoint, path, antLength) {
    let start = path.length - antLength;
    if (path.length < antLength) {
        start = 0;
    }
    if (start < 0) {
        start = 0;
    }

    for (let i = start; i < path.length; i++) {
        if (currentPoint.equals(path[i])) {
            return false;
        }
    }
    return true;
}

function getAllowDirection(currentPoint, lastDirection, board, ant) {
    const sizeBorder = board.size;

    function isWell(element) {
        return element === ELEMENTS.WALL || element === ELEMENTS.START_FLOOR;
    }

    function isOutOfBoard(point) {
        return point.x > sizeBorder - 1 || point.y > sizeBorder - 1 || point.x < 1 || point.y < 1;
    }

    const elementRight = board.getElementAt(currentPoint.shiftRight(1));
    const elementLeft = board.getElementAt(currentPoint.shiftLeft(1));
    const elementUp = board.getElementAt(currentPoint.shiftTop(1));
    const elementDown = board.getElementAt(currentPoint.shiftBottom(1));

    const allowDirection = [];
    const disallowRight = isWell(elementRight) || lastDirection === DIRECTIONS.LEFT || isOutOfBoard(currentPoint.shiftRight(1));

    if (!disallowRight) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.RIGHT,
                'estimate': getEstimateForRandom(currentPoint.shiftRight(1), ant, board)
            });
    }
    const disallowLeft = isWell(elementLeft) || lastDirection === DIRECTIONS.RIGHT || isOutOfBoard(currentPoint.shiftLeft(1));

    if (!disallowLeft) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.LEFT,
                'estimate': getEstimateForRandom(currentPoint.shiftLeft(1), ant, board)
            });
    }
    const disallowUp = isWell(elementUp) || lastDirection === DIRECTIONS.DOWN || isOutOfBoard(currentPoint.shiftTop(1));

    if (!disallowUp) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.UP,
                'estimate': getEstimateForRandom(currentPoint.shiftTop(1), ant, board)
            });
    }
    const disallowDown = isWell(elementDown) || lastDirection === DIRECTIONS.UP || isOutOfBoard(currentPoint.shiftBottom(1));

    if (!disallowDown) {
        allowDirection.push(
            {
                'direction': DIRECTIONS.DOWN,
                'estimate': getEstimateForRandom(currentPoint.shiftBottom(1), ant, board)
            });
    }
    return allowDirection;
}

function getEstimateForRandom(potentialPoint, ant, board) {
    let currentProfit = 0;
    if (isNewPoint(potentialPoint, ant.path)) {
        const estimateTable = getElementsEstimate(ant.berserkMode);
        const gameElement = board.getElementAt(potentialPoint)
        currentProfit = estimateTable[gameElement];
    } else {
        if (isEmptyPoint(potentialPoint, ant.path, ant.antLength)) {
            currentProfit = 0
        } else {
            currentProfit = -5
        }
    }
    return currentProfit;
}

function getDirectionsWithProbability(directions) {
    const total = 1;
    const partForOne = total / directions.length;
    const bestUpper = 0.15;
    const sortedDirections = directions.sort(function (a, b) {
        return b['estimate'] - a['estimate'];
    })
    for (let direction of sortedDirections) {
        direction['probability'] = partForOne;
    }
    if (sortedDirections[0]['estimate'] === sortedDirections[sortedDirections.length - 1]['estimate']) {
        return sortedDirections
    }
    if (sortedDirections.length === 2) {
        sortedDirections[0]['probability'] = partForOne + bestUpper;
        sortedDirections[1]['probability'] = partForOne - bestUpper;
    }
    if (sortedDirections.length === 3) {
        sortedDirections[0]['probability'] = partForOne + bestUpper;
        sortedDirections[1]['probability'] = partForOne;
        sortedDirections[2]['probability'] = partForOne - bestUpper;
    }
    return sortedDirections;
}

function getHeuristicRandomDirection(allowDirections) {
    if (allowDirections.length === 1) {
        return allowDirections[0]["direction"];
    }
    const sortedDirections = getDirectionsWithProbability(allowDirections);
    const rand = Math.random();
    let counterRand = 0;
    for (let direction of sortedDirections) {
        if (rand <= direction['probability'] + counterRand) {
            return direction["direction"];
        } else {
            counterRand += direction['probability']
        }
    }
    console.error('this code should not be used getHeuristicRandomDirection');
    return allowDirections[allowDirections.length - 1]["direction"];
}

function getNextPoint(currentPoint, direction) {
    if (direction === DIRECTIONS.LEFT) {
        return currentPoint.shiftLeft(1)
    }
    if (direction === DIRECTIONS.RIGHT) {
        return currentPoint.shiftRight(1)
    }
    if (direction === DIRECTIONS.UP) {
        return currentPoint.shiftTop(1)
    }
    if (direction === DIRECTIONS.DOWN) {
        return currentPoint.shiftBottom(1)
    }
}


function getBestAnt(antColony) {
    let bestAnt = antColony[0];
    for (let ant of antColony) {
        if (!ant.done) {
            let sum = ant.profitPositive;
            let sumBestAnt = bestAnt.profitPositive;
            if (sum >= sumBestAnt) {
                bestAnt = ant;
            }
        }
    }
    return bestAnt;
}